(function() {
	'use strict';
	//get elements and such.
	nunjucks.configure('/views', { autoescape: true });
	var db = new PouchDB('vault');
	var remoteCouch = 'http://nathan:changeme@192.168.1.104:5984/vault';

	db.changes({
		since: 'now',
		live: true
	}).on('change', showCompanies);

	function sync() {
	  //syncDom.setAttribute('data-sync-state', 'syncing');
		var opts = {live: true};
		db.sync(remoteCouch, opts, syncError);
	}
	 function syncError() {
		console.log("err");
  	}
	function addCompany(companyname){
		var Company = {
			_id: new Date().toISOString(),
			name: companyname
		};
		db.put(Company).then(function(result){
			//call update UI function.
			console.log('Successfully added Company!');
			console.log(result);
		}).catch(function(err){
			console.log("Adding Company failed.");
			console.log(err);
		});
	};
	function addCompanyAdvanced(companyObject){
		console.log("id:", companyObject);
		if (!companyObject._id){
			companyObject._id = new Date().toISOString();
			console.log(' new company');
		}
		db.put(companyObject).then(function(result){
			//call update UI function.
			console.log('Successfully added Company (advanced)!');
			console.log(result);
		}).catch(function(err){
			console.log("Adding Company failed.");
			console.log(err);
		});
	};
	function deleteCompany(company){
		db.remove(company);
	}
	function showDetails(company, mode){
		var detailsList = document.createElement('form');
		detailsList.id = company._rev;
		console.log("show details:",company);
		console.log("mode:",mode);
		$.each(company, function(key, value){
			if (key != '_id' && key != '_rev'){
				var detailTitle = document.createElement('input');
				detailTitle.setAttribute("placeholder", value);
				detailTitle.setAttribute("value", value);
				detailTitle.setAttribute("name", key);
				if (mode !== 'edit'){
					detailTitle.setAttribute("readonly", "readonly");
				}
				detailTitle.className = 'form-control';

				var detailDiv = document.createElement('div');
				detailDiv.className = 'form-group col-md-4';
				var detailLabel = document.createElement('label');
				detailLabel.appendChild(document.createTextNode(key));

				detailDiv.appendChild(detailLabel);
				detailDiv.appendChild(detailTitle);
				//detailValue.appendChild(document.createTextNode(value));

				detailsList.appendChild(detailDiv);
				//detailsList.appendChild(detailValue);
			}
		});
		var container = document.querySelector('#company-details--container');
		container.innerHTML = '';
		if (mode === 'edit'){
			var idField = document.createElement('input');
			idField.setAttribute('type', 'hidden');
			idField.setAttribute('name', '_id');
			idField.setAttribute('value', company._id);
			detailsList.appendChild(idField);

			var revField = document.createElement('input');
			revField.setAttribute('type', 'hidden');
			revField.setAttribute('name', '_rev');
			revField.setAttribute('value', company._rev);
			detailsList.appendChild(revField);

			var saveBtn = document.createElement('button');
			saveBtn.innerHTML = '<i class="fa fa-floppy-o"></i>';
			saveBtn.setAttribute("type", "submit");
			saveBtn.appendChild(document.createTextNode(' Save'));
			saveBtn.className = 'btn btn-primary';
			createListener(detailsList, "submit", createCompanyAdvanced, '#'+company._id);
			detailsList.appendChild(saveBtn);
		}
		container.appendChild(detailsList);

		$('#company-details').modal();
	}
	function createItemControls(company){
		var controls = document.createElement('div');
		controls.className = 'btn-group';
		controls.setAttribute('role','group');
		controls.setAttribute('aria-label','Item Controls');

		var viewButton = document.createElement('button');
		viewButton.innerHTML = '<i class="fa fa-file-text-o"></i>'
		viewButton.className = 'btn btn-primary';
		viewButton.appendChild(document.createTextNode(' View'));
		controls.appendChild(viewButton);
		viewButton.addEventListener( 'click',
			showDetails.bind(this, company));

		var editButton = document.createElement('button');
		editButton.innerHTML = '<i class="fa fa-pencil"></i>'
		editButton.className = 'btn btn-default';
		editButton.appendChild(document.createTextNode(' Edit'));
		controls.appendChild(editButton);
		editButton.addEventListener( 'click',
			showDetails.bind(this, company, 'edit'));

		var delButton = document.createElement('button');
		delButton.innerHTML = '<i class="fa fa-trash-o"></i>'
		delButton.appendChild(document.createTextNode(' Delete'));
		delButton.className = 'btn btn-danger';
		controls.appendChild(delButton);
		delButton.addEventListener( 'click',
			deleteCompany.bind(this, company));

		var controlTd = document.createElement('td');

		controlTd.appendChild(controls);

		return controlTd;
	}
	function createCompanyItem(company){
		//console.log(company);
		var name = document.createElement('td');
		name.appendChild(document.createTextNode(company.name));

		var row = document.createElement('tr');
    row.className = 'view';
    row.appendChild(name);
    row.appendChild(createItemControls(company));
    row.id = 'li_' + company._id;

		return row;
	}

	function renderCompanies(companies){
		var table = document.querySelector('#company-list > tbody');
		table.innerHTML = '';
		companies.forEach(function(company){
			table.appendChild(createCompanyItem(company.doc));
		});
		$('#company-list').DataTable();
	}
	function showCompanies(){
		db.allDocs({include_docs: true, descending: false})
		.then(function(doc){
			renderCompanies(doc.rows);
		}).catch(function(err){
			console.log(err);
			//warn user?
		})
	}
	function createListener(el, action, callback){
		el.addEventListener(action, callback);
	}
	function createCompany(e){
		e.preventDefault();
		var name = company.querySelector("input");
		console.log("company:", name.value);
		if (name.value !== ''){
			addCompany(name.value);
		}
		company.reset();
	}
	function createCompanyAdvanced(e, ele){
		e.preventDefault();
		var o = {};
		console.log('target.id:',e.target.id);
    var a = $('#'+e.target.id).serializeArray();
    $.each(a, function() {
        if (o[this.name] !== undefined) {
            if (!o[this.name].push) {
                o[this.name] = [o[this.name]];

            }
            o[this.name].push(this.value || '');
						console.log('1:',o[this.name]);
        } else {
            o[this.name] = this.value || '';
						console.log('2:',o[this.name]);
        }
    });
		if (o.name !== ''){
			addCompanyAdvanced(o);
		}
		console.log('object: ',o);
		companyAdvanced.reset();
	}

	var company = document.querySelector('#form--create-company');
	var companyAdvanced = document.querySelector('#form--create-company--advanced');

	createListener(company, "submit", createCompany);
	createListener(companyAdvanced, "submit", createCompanyAdvanced, '#form--create-company--advanced');
	showCompanies();
	if (remoteCouch){
		sync();
	}
})();
